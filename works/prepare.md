// Criado - 05/01/2024 | Editado - 05/01/2024

# O ENEM é basicamente o ensino médio inteiro em uma prova separada 
# por tópicos totalizando 180 questões.

# Matérias cobradas no ENEM

- Ciências da Natureza = Biologia, Química e Física; 
- Ciências Humanas = História, Geografia, Filosofia e Sociologia;
- Linguagens, Códigos e suas Tecnologias = Português, Literatura, Língua Estrangeira, Artes, Educação Física e Tecnologias da Informação e Comunicação;
- Matemática e suas Tecnologias = uma porrada de conceito, pega no curso do Ferreto que é sucesso;
- Redação = Basicamente estudar português de um ser humano normal e fazer várias redações.

# Estratégia

- Estudar todo o conteúdo do ano inteiro (3 ano EM);
- Fazer todas as provas dos últimos 5 anos (incluindo temas de redação);
- Fazer UMA PORRADA de redação, afim de ser o próximo nota 1000;
-  
